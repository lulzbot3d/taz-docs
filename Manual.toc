\select@language {english}
\contentsline {chapter}{\emph {WARNINGS}\\ {Safety Information}}{vii}
\contentsline {section}{Read Me First!}{viii}
\contentsline {section}{Hazards and Warnings}{viii}
\contentsline {subsection}{\leavevmode {\color {red}Electric Shock Hazard}}{viii}
\contentsline {subsection}{\leavevmode {\color {red}Burn Hazard}}{viii}
\contentsline {subsection}{\leavevmode {\color {red}Fire Hazard}}{viii}
\contentsline {subsection}{\leavevmode {\color {red}Pinch Hazard}}{viii}
\contentsline {subsection}{\leavevmode {\color {red}Age Warning}}{ix}
\contentsline {subsection}{\leavevmode {\color {red}Modifications and Repairs Warning}}{ix}
\contentsline {section}{Regulatory Compliance Statement Class B}{ix}
\contentsline {subsection}{Federal Communications Commission Statement}{ix}
\contentsline {subsection}{Industry Canada Statement}{x}
\contentsline {subsection}{Australian Communications and Media Authority Statement}{x}
\contentsline {chapter}{\chapternumberline {1}\emph {3D Printer Software}}{11}
\contentsline {section}{\numberline {1.1}\texttt {Software Overview}}{12}
\contentsline {section}{\numberline {1.2}\texttt {Software Types}}{12}
\contentsline {section}{\numberline {1.3}\texttt {CAD and 3D Modeling Software}}{13}
\contentsline {subsection}{\texttt {FreeCAD}}{13}
\contentsline {subsection}{\texttt {OpenSCAD}}{13}
\contentsline {subsection}{\texttt {Blender}}{13}
\contentsline {section}{\numberline {1.4}\texttt {Alternative Printer Host Software}}{14}
\contentsline {subsection}{\texttt {OctoPrint}}{14}
\contentsline {subsection}{\texttt {BotQueue}}{14}
\contentsline {subsection}{\texttt {MatterControl}}{14}
\contentsline {subsection}{\texttt {Source Files}}{14}
\contentsline {chapter}{\chapternumberline {2}\emph {Cura LulzBot Edition}}{15}
\contentsline {section}{\numberline {2.1}\texttt {Cura LulzBot Edition}}{16}
\contentsline {subsection}{\texttt {Installation and Setup}}{16}
\contentsline {section}{\numberline {2.2}\texttt {Recommended Print Settings}}{16}
\contentsline {subsection}{\texttt {Material Selection}}{17}
\contentsline {subsection}{\texttt {Selecting a Quick Print Profile}}{17}
\contentsline {subsubsection}{\texttt {Different Filament Manufacturers}}{18}
\contentsline {subsection}{\texttt {Infill Selection}}{18}
\contentsline {subsection}{\texttt {Printing Support Material}}{18}
\contentsline {subsection}{\texttt {Build Plate Adhesion}}{19}
\contentsline {subsubsection}{\texttt {Skirt}}{19}
\contentsline {subsubsection}{\texttt {Brim}}{19}
\contentsline {subsubsection}{\texttt {Raft}}{19}
\contentsline {subsection}{\texttt {Load Model File}}{19}
\contentsline {subsection}{\texttt {Model Orientation}}{19}
\contentsline {subsubsection}{\texttt {Scale}}{20}
\contentsline {subsubsection}{\texttt {Rotate}}{21}
\contentsline {subsubsection}{\texttt {Lay Flat}}{22}
\contentsline {subsubsection}{\texttt {Reset}}{22}
\contentsline {subsubsection}{\texttt {Mirror}}{22}
\contentsline {subsubsection}{\texttt {Per Model Settings}}{23}
\contentsline {subsubsection}{\texttt {Custom Supports and Support Blocker}}{24}
\contentsline {subsubsection}{\texttt {Multiply Object}}{25}
\contentsline {section}{\numberline {2.3}\texttt {View Options}}{26}
\contentsline {subsection}{\texttt {Solid View}}{26}
\contentsline {subsection}{\texttt {X-Ray View}}{26}
\contentsline {subsection}{\texttt {Layer View}}{27}
\contentsline {section}{\numberline {2.4}\texttt {Monitor Screen}}{28}
\contentsline {subsection}{\texttt {Temperature Control and Monitoring}}{30}
\contentsline {subsection}{\texttt {Active Print}}{30}
\contentsline {subsection}{\texttt {Manual Control}}{30}
\contentsline {subsubsection}{\texttt {Connect, Disconnect, Console}}{30}
\contentsline {subsubsection}{\texttt {Movement, Heating, Extrusion}}{31}
\contentsline {subsection}{\texttt {Predefined Commands}}{31}
\contentsline {section}{\numberline {2.5}\texttt {Starting Your First Print}}{32}
\contentsline {subsection}{\texttt {Printing from USB Cable}}{32}
\contentsline {subsubsection}{\texttt {USB Printing for Multiple Printers}}{32}
\contentsline {subsection}{\texttt {Pausing Mid-Print}}{33}
\contentsline {subsection}{\texttt {Automatic Bed Leveling}}{33}
\contentsline {subsection}{\texttt {Printing from SD Card}}{33}
\contentsline {subsection}{\texttt {Start Print}}{33}
\contentsline {subsection}{\texttt {Recommended Temperatures and Bed Preparation}}{34}
\contentsline {section}{\numberline {2.6}\texttt {Removing Your First Print}}{34}
\contentsline {section}{\numberline {2.7}\texttt {Custom Settings}}{35}
\contentsline {subsection}{\texttt {Setting Visibility}}{36}
\contentsline {section}{\numberline {2.8}\texttt {Quality Tab Settings}}{36}
\contentsline {subsubsection}{\texttt {Layer Height}}{36}
\contentsline {subsubsection}{\texttt {Initial Layer Height}}{37}
\contentsline {subsubsection}{\texttt {Line Width}}{37}
\contentsline {subsubsection}{\texttt {Backlash Fading Distance}}{38}
\contentsline {section}{\numberline {2.9}\texttt {Shell Tab Settings}}{39}
\contentsline {subsubsection}{\texttt {Wall Extruder}}{39}
\contentsline {subsubsection}{\texttt {Wall Thickness}}{39}
\contentsline {subsubsection}{\texttt {Outer Wall Wipe Distance}}{40}
\contentsline {subsubsection}{\texttt {Top Surface Skin Layers}}{40}
\contentsline {subsubsection}{\texttt {Top/Bottom Extruder, Thickness, Pattern}}{40}
\contentsline {subsubsection}{\texttt {Outer Wall Inset}}{40}
\contentsline {subsubsection}{\texttt {Optimize Wall Printing Order}}{40}
\contentsline {subsubsection}{\texttt {Outer Before Inner Walls}}{41}
\contentsline {subsubsection}{\texttt {Alternate Extra Wall}}{41}
\contentsline {subsubsection}{\texttt {Compensate Inner/Outer Wall Overlaps}}{41}
\contentsline {subsubsection}{\texttt {Minimum Wall Flow}}{41}
\contentsline {subsubsection}{\texttt {Fill Gaps Between Walls}}{41}
\contentsline {subsubsection}{\texttt {Filter Out Tiny Gaps}}{42}
\contentsline {subsubsection}{\texttt {Print Thin Walls}}{42}
\contentsline {subsubsection}{\texttt {Horizontal Expansion}}{42}
\contentsline {subsubsection}{\texttt {Z Seam Alignment}}{42}
\contentsline {subsubsection}{\texttt {Seam Corner Preference}}{43}
\contentsline {subsubsection}{\texttt {Ignore Small Z Gaps}}{43}
\contentsline {subsubsection}{\texttt {Extra Skin Wall Count}}{43}
\contentsline {subsubsection}{\texttt {Ironing}}{43}
\contentsline {section}{\numberline {2.10}\texttt {Infill Tab Settings}}{44}
\contentsline {subsection}{\texttt {Infill Extruder}}{44}
\contentsline {subsection}{\texttt {Infill Density}}{44}
\contentsline {subsection}{\texttt {Infill Pattern}}{45}
\contentsline {subsection}{\texttt {Connect Infill Lines}}{46}
\contentsline {subsection}{\texttt {Infill Line Directions}}{46}
\contentsline {subsection}{\texttt {Infill Offset Directions}}{46}
\contentsline {subsection}{\texttt {Infill Line Multiplier}}{47}
\contentsline {subsection}{\texttt {Infill and Skin Overlap Percentage}}{47}
\contentsline {subsection}{\texttt {Infill Wipe Distance}}{47}
\contentsline {subsection}{\texttt {Infill Layer Thickness}}{47}
\contentsline {subsection}{\texttt {Gradual Infill Steps and Step Height}}{47}
\contentsline {subsection}{\texttt {Infill Before Walls}}{48}
\contentsline {subsection}{\texttt {Infill Support}}{48}
\contentsline {subsection}{\texttt {Skin Removal Width}}{48}
\contentsline {subsection}{\texttt {Skin Expand Distance}}{48}
\contentsline {subsubsection}{\texttt {Maximum Skin Angle for Expansion}}{48}
\contentsline {section}{\numberline {2.11}\texttt {Material Settings Tab}}{49}
\contentsline {subsection}{\texttt {Default Printing Temperature}}{49}
\contentsline {subsection}{\texttt {Printing Temperature}}{49}
\contentsline {subsubsection}{\texttt {Probe Temperature}}{49}
\contentsline {subsubsection}{\texttt {Soften Temperature}}{49}
\contentsline {subsubsection}{\texttt {Wipe Temperature}}{49}
\contentsline {subsection}{\texttt {Printing Temperature Initial Layer}}{50}
\contentsline {subsection}{\texttt {Initial Printing Temperature}}{50}
\contentsline {subsection}{\texttt {Final Printing Temperature}}{50}
\contentsline {subsection}{\texttt {Extrusion Cool Down Speed Modifier}}{50}
\contentsline {subsection}{\texttt {Build Plate Temperature}}{50}
\contentsline {subsubsection}{\texttt {Part Removal Temperature}}{51}
\contentsline {subsubsection}{\texttt {Keep Heating}}{51}
\contentsline {subsubsection}{\texttt {Build Plate Temperature Initial Layer}}{51}
\contentsline {subsection}{\texttt {Diameter}}{51}
\contentsline {subsection}{\texttt {Flow Rate}}{51}
\contentsline {subsection}{\texttt {Retraction}}{52}
\contentsline {subsubsection}{\texttt {Enable Retraction}}{52}
\contentsline {subsubsection}{\texttt {Retract at Layer Change}}{52}
\contentsline {subsubsection}{\texttt {Retraction Distance}}{52}
\contentsline {subsubsection}{\texttt {Retraction Speeds}}{52}
\contentsline {subsection}{\texttt {Retraction Extra Prime Amount}}{52}
\contentsline {subsection}{\texttt {Maximum Retraction Count}}{53}
\contentsline {subsection}{\texttt {Standby Temperature}}{53}
\contentsline {subsection}{\texttt {Nozzle Switch Retraction Distance and Speeds}}{53}
\contentsline {section}{\numberline {2.12}\texttt {Speed Settings Tab}}{53}
\contentsline {subsection}{\texttt {Print Speed}}{53}
\contentsline {subsubsection}{\texttt {Infill Speed}}{54}
\contentsline {subsubsection}{\texttt {Wall Speeds}}{54}
\contentsline {subsubsection}{\texttt {Top/Bottom Speed}}{54}
\contentsline {subsubsection}{\texttt {Initial Layer Speeds}}{54}
\contentsline {subsubsection}{\texttt {Skirt/Brim Speed}}{54}
\contentsline {subsubsection}{\texttt {Maximum Z Speed}}{54}
\contentsline {subsubsection}{\texttt {Number of Slower Layers}}{55}
\contentsline {subsubsection}{\texttt {Equalize Filament Flow}}{55}
\contentsline {subsubsection}{\texttt {Enable Acceleration and Jerk Control}}{55}
\contentsline {section}{\numberline {2.13}\texttt {Travel Tab Settings}}{55}
\contentsline {subsection}{\texttt {Combing Mode}}{55}
\contentsline {subsection}{\texttt {Avoid Printed Parts When Traveling}}{56}
\contentsline {subsection}{\texttt {Layer Start}}{56}
\contentsline {subsection}{\texttt {Z Hop}}{56}
\contentsline {section}{\numberline {2.14}\texttt {Cooling Settings Tab}}{56}
\contentsline {subsection}{\texttt {Enable Print Cooling}}{57}
\contentsline {subsubsection}{\texttt {Fan Speed Settings}}{57}
\contentsline {subsection}{\texttt {Minimum Layer Time, Speed, and Lift Head}}{58}
\contentsline {section}{\numberline {2.15}\texttt {Support Tab Settings}}{58}
\contentsline {subsection}{\texttt {Generate Support}}{58}
\contentsline {subsection}{\texttt {Support Options}}{59}
\contentsline {subsection}{\texttt {Support Overhang Angle}}{60}
\contentsline {subsection}{\texttt {Support Pattern}}{60}
\contentsline {subsection}{\texttt {Support Density}}{60}
\contentsline {subsection}{\texttt {Enable Support Brim}}{61}
\contentsline {subsection}{\texttt {Support Infill Line Direction}}{61}
\contentsline {subsection}{\texttt {Support Distances}}{61}
\contentsline {subsection}{\texttt {Enable Support Interface}}{62}
\contentsline {subsection}{\texttt {Tower Support Settings}}{63}
\contentsline {subsubsection}{\texttt {Tower Diameter}}{63}
\contentsline {subsubsection}{\texttt {Minimum Diameter}}{63}
\contentsline {subsubsection}{\texttt {Tower Roof Angle}}{63}
\contentsline {section}{\numberline {2.16}\texttt {Build Plate Adhesion Settings Tab}}{63}
\contentsline {subsubsection}{\texttt {Skirt}}{63}
\contentsline {subsubsection}{\texttt {Brim}}{64}
\contentsline {subsubsection}{\texttt {Raft}}{64}
\contentsline {subsubsection}{\texttt {None}}{64}
\contentsline {section}{\numberline {2.17}\texttt {Dual Extrusion Settings Tab}}{64}
\contentsline {subsection}{\texttt {Prime Tower}}{64}
\contentsline {subsection}{\texttt {Ooze Shield}}{65}
\contentsline {section}{\numberline {2.18}\texttt {Mesh Fixes Settings Tab}}{65}
\contentsline {subsection}{\texttt {Union Overlapping Volumes}}{66}
\contentsline {subsection}{\texttt {Remove All Holes}}{66}
\contentsline {subsection}{\texttt {Extensive Stitching}}{66}
\contentsline {subsection}{\texttt {Keep Disconnected Faces}}{66}
\contentsline {subsection}{\texttt {Merged Meshes Overlap}}{66}
\contentsline {subsection}{\texttt {Remove Mesh Intersection}}{66}
\contentsline {subsection}{\texttt {Alternate Mesh Removal}}{66}
\contentsline {section}{\numberline {2.19}\texttt {Special Modes Settings Tab}}{67}
\contentsline {subsection}{\texttt {Mold}}{67}
\contentsline {subsubsection}{\texttt {Minimal Mold Width}}{67}
\contentsline {subsubsection}{\texttt {Mold Roof Height}}{67}
\contentsline {subsubsection}{\texttt {Mold Angle}}{67}
\contentsline {subsection}{\texttt {Surface Mode}}{67}
\contentsline {subsubsection}{\texttt {Surface Mode}}{67}
\contentsline {subsubsection}{\texttt {Both Mode}}{68}
\contentsline {subsection}{\texttt {Spiralize Outer Contour}}{68}
\contentsline {subsection}{\texttt {Relative Extrusion}}{68}
\contentsline {section}{\numberline {2.20}\texttt {Experimental Settings Tab}}{68}
\contentsline {subsection}{\texttt {Tree Support}}{68}
\contentsline {subsection}{\texttt {Slicing Tolerance}}{69}
\contentsline {subsection}{\texttt {Infill Travel Optimization}}{69}
\contentsline {subsection}{\texttt {Minimum Polygon Circumference}}{70}
\contentsline {subsection}{\texttt {Maximum Resolution}}{70}
\contentsline {subsection}{\texttt {Maximum Travel Resolution}}{70}
\contentsline {subsection}{\texttt {Break Up Support In Chunks}}{70}
\contentsline {subsection}{\texttt {Enable Draft Shield}}{70}
\contentsline {subsection}{\texttt {Make Overhang Printable}}{70}
\contentsline {subsection}{\texttt {Enable Coasting}}{71}
\contentsline {subsection}{\texttt {Alternate Skin Rotation}}{71}
\contentsline {subsection}{\texttt {Spaghetti Infill}}{71}
\contentsline {subsection}{\texttt {Enable Conical Support}}{71}
\contentsline {subsection}{\texttt {Fuzzy Skin}}{72}
\contentsline {subsection}{\texttt {Flow Rate Compensation}}{72}
\contentsline {subsection}{\texttt {Wire Frame Printing}}{72}
\contentsline {subsection}{\texttt {Use Adaptive Layers}}{72}
\contentsline {subsection}{\texttt {Overhang Wall}}{73}
\contentsline {subsection}{\texttt {Enable Bridge Settings}}{73}
\contentsline {chapter}{\chapternumberline {3}\emph {Maintaining Your 3D Printer}}{75}
\contentsline {section}{\numberline {3.1}\texttt {Overview}}{76}
\contentsline {section}{\numberline {3.2}\texttt {Smooth Rods}}{76}
\contentsline {section}{\numberline {3.3}\texttt {PEI Print Surface}}{76}
\contentsline {section}{\numberline {3.4}\texttt {Hobbed Bolt}}{76}
\contentsline {section}{\numberline {3.5}\texttt {Belts}}{77}
\contentsline {section}{\numberline {3.6}\texttt {Hot End}}{77}
\contentsline {section}{\numberline {3.7}\texttt {Nozzle Wiping Pad}}{77}
\contentsline {section}{\numberline {3.8}\texttt {Bed Leveling Washers and Calibration Cube}}{78}
\contentsline {section}{\numberline {3.9}\texttt {Cooling Fans}}{78}
\contentsline {section}{\numberline {3.10}\texttt {Control Box}}{78}
\contentsline {chapter}{\chapternumberline {4}\emph {Advanced Usage}}{79}
\contentsline {section}{\numberline {4.1}\texttt {Intro}}{80}
\contentsline {section}{\numberline {4.2}Changing nozzles}{80}
\contentsline {section}{\numberline {4.3}Bed Adhesion}{80}
\contentsline {subsection}{Glue stick/PVA Glue solution}{80}
\contentsline {chapter}{\chapternumberline {5}\emph {Troubleshooting}}{81}
\contentsline {section}{\numberline {5.1}\texttt {Troubleshooting}}{82}
\contentsline {subsection}{Error Codes}{82}
\contentsline {subsubsection}{\texttt {MINTEMP}}{82}
\contentsline {subsubsection}{\texttt {MAXTEMP}}{82}
\contentsline {subsubsection}{\texttt {Thermal Error E1}}{82}
\contentsline {subsubsection}{\texttt {Thermal Error E2}}{82}
\contentsline {subsubsection}{\texttt {Thermal Error Bed}}{82}
\contentsline {subsubsection}{\texttt {PROBE FAIL CLEAN NOZZLE}}{83}
\contentsline {subsubsection}{\texttt {HEATING FAILED}}{83}
\contentsline {chapter}{\chapternumberline {6}\emph {Hardware and Software Source Code}}{85}
\contentsline {chapter}{\chapternumberline {7}\emph {3D Printer Support}}{87}
\contentsline {section}{\numberline {7.1}LulzBot}{88}
\contentsline {section}{\numberline {7.2}Support}{88}
\contentsline {section}{\numberline {7.3}Regional Phone Numbers}{88}
\contentsline {section}{\numberline {7.4}Community}{88}
\contentsline {chapter}{\chapternumberline {8}\emph {Warranty Information}}{89}
\contentsline {section}{\numberline {8.1}Warranty}{90}
\contentsline {subsection}{Extended Warranty}{90}
\contentsline {section}{\numberline {8.2}Modification Warning}{90}
\contentsline {chapter}{\chapternumberline {9}\emph {Contact Information}}{91}
\contentsline {section}{\numberline {9.1}Support}{92}
\contentsline {section}{\numberline {9.2}Sales}{92}
\contentsline {section}{\numberline {9.3}Regional Phone Numbers}{92}
\contentsline {section}{\numberline {9.4}Websites}{93}
\contentsline {chapter}{Index}{95}
\contentsline {chapter}{Glossary}{101}
\contentsline {section}{\texttt {Notes}}{105}
